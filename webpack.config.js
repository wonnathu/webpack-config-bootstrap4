const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

const devMode = (process.env.NODE_ENV !== 'production');

module.exports = {
    mode: 'development',
    entry: {
        app: [
            './src/js/app.js',
            './src/sass/app.scss',
        ],
    },
    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: 'js/[name].js'
    },
    module: {
        rules: [{
                test: /\.s[ac]ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            //
                        },
                    },
                    {
                        loader: 'resolve-url-loader',
                        options: {
                            //
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sourceMapContents: false,
                        }
                    },
                ],
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: '[name]-[hash].[ext]',
                    outputPath: 'images',
                    publicPath: '../images',
                },
            },
            {
                test: /\.js$/,
                exclude: '/node_modules',
                loader: "babel-loader"
            }
        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // all options are optional
            filename: 'css/[name].css',
            chunkFilename: '[id].css',
            ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
    ],

    optimization: {
        minimizer: [
            new OptimizeCSSAssetsPlugin({}),
            new TerserPlugin({
                exclude: '/node_modules',
            }),
        ]
    }
};
if (process.env.NODE_ENV === 'production') {
    module.exports.mode = 'production';
}